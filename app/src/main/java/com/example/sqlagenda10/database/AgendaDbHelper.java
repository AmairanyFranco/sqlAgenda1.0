package com.example.sqlagenda10.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class AgendaDbHelper extends SQLiteOpenHelper {
    private static final String  TEXT_TYPE = " TEXT";
    private static final String INTERGER_TYPE = " INTERGER";
    private static final String COMMA = " ,";
    private static final String SQL_CREATE_CONTACTO = " CREATE TABLE " + DefinirTable.Contacto.TABLE_NAME + " (" +
            DefinirTable.Contacto._ID + " INTERGER PRIMARY KEY, " +
            DefinirTable.Contacto.NOMBRE + TEXT_TYPE + COMMA +
            DefinirTable.Contacto.DOMICILIO + TEXT_TYPE + COMMA +
            DefinirTable.Contacto.TELEFONO1 + TEXT_TYPE + COMMA +
            DefinirTable.Contacto.TELEFONO2 + TEXT_TYPE + COMMA +
            DefinirTable.Contacto.NOTAS + TEXT_TYPE + COMMA +
            DefinirTable.Contacto.FAVORITO + INTERGER_TYPE + ")";
    public static final String SQL_DELETE_CONTACTO = "DROP TABLE IF EXISTS " + DefinirTable.Contacto.TABLE_NAME;
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "agenda.db";


    public AgendaDbHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
        //Funcion de borrar
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_CONTACTO);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_CONTACTO);
        onCreate(db);

    }
}
